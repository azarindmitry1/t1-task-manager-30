package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-start-by-index";

    @NotNull
    public final static String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final String userId = getUserId();
        serviceLocator.getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
